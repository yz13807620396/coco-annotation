import paper from "paper";

export default {
  model: {
    prop: "selected",
    event: "update",
  },
  props: {
    selected: {
      type: String,
      required: true,
    },
  },
  template:
    "<div><i v-tooltip.right='tooltip' class='fa fa-x' :class='icon' :style='{ color: iconColor }' @click='click'></i><br></div>",
  data() {
    return {
      tool: null,
      enabled: false,
      cursor: "default",
      color: {
        enabled: "gray",
        active: "red",
        disabled: "gray",
        toggle: "red",
      },
    };
  },
  methods: {
    onMouseMove() { },
    onMouseDown() { },
    onMouseDrag() { },
    onMouseUp() { },
    click() {
      this.update();
      this.$parent.showAutoAnnotationOption = false;
      this.$parent.showEnhancementOption = false;
    },
    update() {
      if (this.isDisabled) return;
      this.$emit("update", this.name);
    },
    setPreferences() { },
  },
  computed: {
    isActive() {
      if (this.selected == this.name) {
        this.$emit("setcursor", this.cursor);
        return true;
      }
      return false;
    },
    iconColor() {
      if (this.isDisabled) return this.color.disabled;

      if (this.isToggled) return this.color.toggle;
      if (this.isActive) return this.color.active;

      return this.color.enabled;
    },
    isDisabled() {
      return false;
    },
    tooltip() {
      if (this.isDisabled) {
        // return this.name + " (select an annotation to activate tool)";
        return this.name + " (请先新建一个标注)";
      }
      // return this.name + " Tool";
      return this.name;
    },
  },
  watch: {
    isActive(active) {
      if (active) {
        this.tool.activate();
      }
    },
    isDisabled(disabled) {
      if (disabled && this.isActive) {
        this.$emit("update", "Select");
      }
    },
  },
  mounted() {
    this.tool = new paper.Tool();

    this.tool.onMouseDown = this.onMouseDown;
    this.tool.onMouseDrag = this.onMouseDrag;
    this.tool.onMouseMove = this.onMouseMove;
    this.tool.onMouseUp = this.onMouseUp;
  },
};
