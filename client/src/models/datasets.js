import axios from "axios";

const baseURL = "/api/dataset";

export default {
  allData(params) {
    return axios.get(`${baseURL}/data`, {
      params: {
        ...params,
      },
    });
  },
  getData(id, params) {
    return axios.get(`${baseURL}/${id}/data`, {
      params: {
        ...params,
      },
    });
  },
  create(name, categories, type) {
    console.log(name+" "+categories+" "+type)
    return axios.post(`${baseURL}/?name=${name}`, {
      categories: categories,
      type: type
    });
  },
  generate(id, body) {
    return axios.post(`${baseURL}/${id}/generate`, {
      ...body,
    });
  },
  scan(id) {
    return axios.get(`${baseURL}/${id}/scan`);
  },
  exportingCOCO(id, categories, with_empty_images) {
    return axios.get(
      `${baseURL}/${id}/export?categories=${categories}&with_empty_images=${with_empty_images}`
    );
  },
  getCoco(id) {
    return axios.get(`${baseURL}/${id}/coco`);
  },
  sendBatchTask(id, categories) {
    return axios.post(`/api/annotation/batch`, {
      dataset_id: id,
      category_ids: categories,
      metadata: "",
      box_treshold: 0.35,
      text_treshold: 0.25,
    });
  },
  uploadImage(datasetId, file) {
    let form = new FormData();
    form.append("image", file);
    return axios.post(`/api/image/?dataset_id=${datasetId}`, form, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  uploadCoco(id, file) {
    let form = new FormData();
    form.append("coco", file);

    return axios.post(`${baseURL}/${id}/coco`, form, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  export(id, format) {
    return axios.get(`${baseURL}/${id}/${format}`);
  },
  getUsers(id) {
    return axios.get(`${baseURL}/${id}/users`);
  },
  getStats(id) {
    return axios.get(`${baseURL}/${id}/stats`);
  },
  getExports(id) {
    return axios.get(`${baseURL}/${id}/exports`);
  },
  resetMetadata(id) {
    return axios.get(`${baseURL}/${id}/reset/metadata`);
  },
  getChildrenPath(id, path = "") {
    return axios.get(`${baseURL}/makedir?dataset_id=${id}&folder=${path}`);
  },
  postNewdir(id, path = "", child) {
    // return axios.post(
    //   `${baseURL}/makedir?dataset_id=${id}&folder=${path}&new_folder=${child}`
    // );
    console.log(
      `${baseURL}/makedir?dataset_id=${id}&folder=${path}&new_folder=${child}`
    );
    let form = new FormData();
    form.append("dataset_id", String(id));
    form.append("folder", path);
    form.append("new_folder", child);
    return axios.post(`${baseURL}/makedir`, form);
  },
};
