import axios from "axios";

const baseURL = "/api/export";

export default {
  download(id, dataset, type = "") {
    if (type === "coco") type = "";
    console.log(`${baseURL}/${id}/download?dataset=${dataset}&format=${type}`);
    axios({
      url: `${baseURL}/${id}/download?anno_format=${type}`,
      method: "GET",
      responseType: "blob",
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      console.log(url);
      console.log(response.data);
      const link = document.createElement("a");
      link.href = url;
      if (type === "") link.setAttribute("download", `${dataset}-${id}.json`);
      else link.setAttribute("download", `${dataset}-${id}.zip`);
      document.body.appendChild(link);
      link.click();
    });
  },
};
