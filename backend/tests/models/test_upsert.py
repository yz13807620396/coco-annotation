import os
import shutil

from database import CategoryModel, upsert, ImageModel,AnnotationModel,DatasetModel
import logging
from mongoengine import *
logger = logging.getLogger('gunicorn.error')
category1 = {
    "name": "Upsert Category",
    "color": "white"
}

def test_zxj():
    images = ImageModel.objects(deleted=False, dataset_id=7)
    for image in images:
        image.annotated = True
        image.num_annotations = 1
        image.save()

def test_2():
    image_id = 247
    image = ImageModel.objects(id=image_id).first()
    # todo 生成图片

    dataset = image.dataset_id
    dataset_path = DatasetModel.objects(id=dataset).first().directory
    enhance_directory = dataset_path + r'enhance/'
    if not os.path.exists(enhance_directory):
        os.makedirs(enhance_directory)
    enhance_filepath = os.path.join(enhance_directory, image.file_name)
    #todo mock增强操作
    shutil.copyfile(image.path, enhance_filepath)
    image = ImageModel.create_from_path(enhance_filepath)
    image.enhanced = True
    image.save()

def test_auto():
    images = ImageModel.objects(deleted=False,dataset_id=7)
    for image in images:
        try:
            annotation = AnnotationModel(
                image_id=image.id,
                category_id=83,
                metadata=DictField(),
                area=345960,
                name="cat",
                visualize=True,
                width = image.width,
                height = image.height,
                segmentation=[
                            1491.7,
                            636.6,
                            1491.7,
                            1194.9,
                            872.0,
                            1194.9,
                            872.0,
                            636.6
                        ],
                keypoints=[],
                bbox=[
                        872.0,
                        637.0,
                        620.0,
                        558.0
                    ],
                isbbox=True
            )
            annotation.save()
            logger.info('ok\n')
        except (ValueError, TypeError) as e:
            logger.info('fail\n')
            return {'message': str(e)}, 400


class TestCategoryUpsert:

    def test_create_category(self):
        query = { "name": category1.get("name") }
        create_category1 = upsert(CategoryModel, query=query, update=category1)
        logger.info('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaas')
        assert create_category1.name == category1.get("name")
        assert create_category1.color == category1.get("color")

        found = CategoryModel.objects(**query).first()
        assert found.name == category1.get("name")
        assert found.color == category1.get("color")

    def test_update_category(self):
        query = {"name": category1.get("name")}
        set = {"name": "Upsert New", "color": "black"}

        found = upsert(CategoryModel, query=query, update=set)

        assert found.name == set.get("name")
        assert found.color == set.get("color")

