from database import ImageModel
from celery import task, shared_task
import logging

logger = logging.getLogger('gunicorn.error')

@shared_task
def thumbnail_generate_single_image(image_id):
    logger.info("tasks/thumbnails.py 中的 thumbnail_generate_single_image")
    image = ImageModel.objects(id=image_id).first()
    image.thumbnail()
    image.flag_thumbnail(flag=False)


__all__ = ["thumbnail_generate_single_image"]