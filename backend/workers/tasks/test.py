
from celery import shared_task
from database import TaskModel

from ..socket import create_socket


@shared_task
def long_task(n, task_id):

    task = TaskModel.objects.get(id=task_id)
    task.update(status="PROGRESS")

    socketio = create_socket()

    print(f"This task will take {n} seconds")
    import time

    for i in range(n):
        print(i)
        time.sleep(1)
        socketio.emit('test', i)
    
    return n


# from flask import Flask, request
# import cv2
# import numpy as np
#
# app = Flask(__name)
#
# @app.route('/receive_image', methods=['POST'])
# def receive_image():
#     file = request.files['image']
#     image_data = file.read()
#     nparr = np.fromstring(image_data, np.uint8)
#     image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
#
#     # 在这里可以对接收到的图像数据进行处理
#     # ...
#
#     return 'Image received successfully'
#
# if __name__ == '__main__':
#     app.run()


__all__ = ["long_task"]