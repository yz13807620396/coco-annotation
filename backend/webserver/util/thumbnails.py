from database import ImageModel
import logging

logger = logging.getLogger('gunicorn.error')

def generate_thumbnails():
    PREFIX = "[Thumbnails]"
    print(f'{PREFIX} Sending request for regenerating images with non actual thumbnails', flush=True)
    [generate_thumbnail(image) for image in ImageModel.objects(regenerate_thumbnail=True).all()]


def generate_thumbnail(image):
    logger.info("thumbnails.py 中的 generate_thumbnail")
    from workers.tasks import thumbnail_generate_single_image
    thumbnail_generate_single_image.delay(image.id)
