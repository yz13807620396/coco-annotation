import requests


def upload_image_to_remote_flask(local_image_path, remote_flask_url):
    try:
        # 打开本地图片文件
        with open(local_image_path, 'rb') as f:
            # 准备发送的数据
            files = {'file': f}
            # 发送POST请求到远程Flask应用
            response = requests.post(remote_flask_url, files=files)
            # 返回响应内容
            return response.text
    except Exception as e:
        return str(e)


# 测试函数
if __name__ == "__main__":
    local_image_path = '../17785983.jpg'  # 本地图片文件路径
    remote_flask_url = 'http://125.220.157.228:29994/inpaint/upload'  # 远程Flask应用的URL
    response = upload_image_to_remote_flask(local_image_path, remote_flask_url)
    print(response)