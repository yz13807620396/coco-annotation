from bing_image_downloader import downloader
from threading import Thread


# downloader.download('cat', limit=10,  output_dir='downloads', adult_filter_off=True, force_replace=False, timeout=60, verbose=True)

def download_bing_images(output_dir):
    downloader.download('milk',
                        limit=10,
                        output_dir=output_dir,
                        adult_filter_off=True,
                        force_replace=False,
                        timeout=10,
                        verbose=True)

thread = Thread(target=download_bing_images, args=('downloads',))
thread.start()
